﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_Gewinnt
{
    public partial class Form1 : Form
    {
        string spieler = "x";
        Logik vg = new Logik();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button clicked = sender as Button;
            clicked.Text = spieler;
            clicked.Enabled = false;

            string nr = clicked.Name.Substring(6);
            int n = Convert.ToInt32(nr);
            vg.Setze(n, spieler);

            string sieger = vg.Gewinnermittlung();
            if (sieger != null)
            {
                MessageBox.Show("Sieger : " + sieger);
            }

            spieler = spieler == "x" ? "o" : "x"; 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
