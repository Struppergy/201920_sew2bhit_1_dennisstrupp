﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace _4_Gewinnt
{
    interface ISpielbar
    {
        void Setze(int nr, string wert);
        string Gewinnermittlung();
    }


    class Logik : ISpielbar
    {
        static int XSIZE = 7, YSIZE = 6;
        private string[,] feld = new string[XSIZE, YSIZE];
        public string Gewinnermittlung()
        {
            string g;
            for (int i = 0; i < YSIZE; i++)
            {
                g = GewinnWaagrecht(i);
                if (g != null)
                {
                    return g;
                }
            }

            for (int i = 0; i < XSIZE; i++)
            {
                g = GewinnSenkrecht(i);
                if (g != null)
                {
                    return g;
                }
            }

            g = HauptDiagonale();
            if (g != null)
            {
                return g;
            }

            g = NebenDiagonale();
            if (g != null)
            {
                return g;
            }

            return null;
        }

        public void Setze(int nr, string wert)
        {
            int x = (nr - 1) % XSIZE;
            int y = (nr - 1) / XSIZE;
            feld[x, y] = wert;

        }

        private string GewinnWaagrecht(int zeile)
        {
            for (int x = 0; x < 4; x++)
            {
                if (feld [x,zeile] != null)
                {
                    if (feld [x,zeile] == feld [x+1, zeile] 
                        && feld[x,zeile] == feld [x+2 , zeile] 
                        && feld[x, zeile] == feld[x+3, zeile])
                    {
                        return feld[x, zeile];
                    }
                }
            }
            return null;
        }
        private string GewinnSenkrecht(int spalte)

        {
            for (int y = 0; y < 3; y++)
            {
                if (feld[spalte, y] == feld[spalte, y+1]
                        && feld[spalte, y] == feld[spalte, y +2]
                        && feld[spalte, y] == feld[spalte, y+3])
                {
                    return feld[spalte, y];
                }
            }
            return null;
        }
        private string HauptDiagonale()
        {
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (feld [x,y] != null)
                    {
                        if (feld[x, y] == feld[x + 1, y+1]
                        && feld[x, y] == feld[x + 2, y+2]
                        && feld[x, y] == feld[x + 3, y+3])
                        
                            return feld[x, y];
                        
                    }
                }
            }
            return null;

        }
        private string NebenDiagonale()
        {
            for (int x = 6; x > 2; x--)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (feld[x, y] == feld[x - 1, y + 1]
                        && feld[x, y] == feld[x - 2, y + 2]
                        && feld[x, y] == feld[x - 3, y + 3])
                    
                        return feld[x, y];
                 }
            
             }
            return null;
        }


    }
}
