﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TikTakToe
{
    public partial class TicTacToe : Form
    {
        bool turn = true;
        int countturn = 0;
        public TicTacToe()
        {
            InitializeComponent();
        }

        private void TicTacToe_Load(object sender, EventArgs e)
        {

        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn == true)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";  
            }
           
            b.Enabled = false;
            check(); 
            turn = !(turn);
        }

        private void check()
        {
            bool winner = false;
            // Das ist der Check für die Gewinnermittlung horizontral
            if ((button1.Text == button2.Text)&&(button2.Text==button3.Text)&&(!button1.Enabled))
            {
                winner = true;
                countturn--;
            }
            else if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (!button4.Enabled))
            {
                winner = true;
                countturn--;
            }
            else if ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (!button7.Enabled))
            {
                winner = true;
                countturn--;
            }
            
            //Das ist der Check für die Gewinnermittlung diagonal

            else if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (!button1.Enabled))
            {
                winner = true;
                countturn--;
            }
            else if ((button3.Text == button5.Text) && (button5.Text == button7.Text) && (!button7.Enabled))
            {
                winner = true;
                countturn--;
            }
            
            //Das ist der Check für die Gewinnermittlung vertikal 

            else if ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (!button1.Enabled))
            {
                winner = true;
                countturn--;
            }
            else if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (!button2.Enabled))
            {
                winner = true;
                countturn--;
            }
            else if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (!button3.Enabled))
            {
                winner = true;
                countturn--;
            }
            if (winner == true)
            {
                string winnername = "";

                if (turn == true)
                {
                    winnername = "X";
                }
                else
                {
                    winnername = "O";
                }
                MessageBox.Show(winnername + " wins");
                buttondisable();
            }
            if (countturn == 8)
            {
                MessageBox.Show("Draw!");
            }
            countturn++;
        }
          
        private void buttondisable()
        {
            //hier habe ich das try/catch gesetzt damit alle Buttons disabled wird und keine Fehlermeldung kommt, 
            //da VS auch die Menüleiste disablen würde und dann sagt VS das ein menükasterl nicht in einen Button umgewandelt werden kann
            
            try
            {
            foreach (Control c in Controls)
            {
                Button b = (Button)c;
                b.Enabled = false; 
            }
            }
            catch { }
            
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            // Ich benutze try/catch damit kein Fehler aufplopt und das Programm sich nicht schließt. Somit resetet sich das Game ohne Probleme. 
            try
            {
                foreach (Control y in Controls)
                {
                    Button b = (Button)y;
                    b.Enabled = true;
                    b.Text = "";
                    countturn = 0;
                }
            }
            catch { }
            
               
                
           
        }
    }
}
